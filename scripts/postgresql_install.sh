#!/bin/bash

echo "[TASK 1] Create Boudary role with database"
sudo su postgres -c "psql -c \"CREATE ROLE boundary SUPERUSER LOGIN PASSWORD 'boundary'\" " >/dev/null 2>&1
sudo su postgres -c "createdb -E UTF8 -T template0 --locale=en_US.utf8 -O boundary boundary" >/dev/null 2>&1