listener "tcp" {
    purpose = "proxy"
    tls_disable = true
    address = "10.10.0.20"
}

worker {
  # Name attr must be unique across workers
  name = "demo-worker-1"
  description = "A default worker created demonstration"

  # Workers must be able to reach controllers on :9201
  controllers = [
    "10.7.0.10",
    "10.7.0.11"
  ]

  tags {
    type   = ["dev", "webservers"]
    region = ["eu-central-1"]
  }
}

# Worker authorization KMS
# Use a production KMS such as AWS KMS for production installs
# This key is the same key used in the worker configuration
kms "aead" {
  purpose = "worker-auth"
  aead_type = "aes-gcm"
  key = "8fZBjCUfN0TzjEGLQldGY4+iE9AkOvCfjh7+p0GtRBQ="
  key_id = "global_worker-auth"
}